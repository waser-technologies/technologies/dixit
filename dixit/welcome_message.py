"""
The welcome message. This is displayed when the editor opens without any files.
"""
from __future__ import unicode_literals
from prompt_toolkit.formatted_text.utils import fragment_list_len

import prompt_toolkit
import dixit
import platform
import sys
version = sys.version_info
dixit_version = dixit.__version__

__all__ = (
    'HEADLINE',
    'WELCOME_MESSAGE_TOKENS',
    'WELCOME_MESSAGE_WIDTH',
    'WELCOME_MESSAGE_HEIGHT'
)

WELCOME_MESSAGE_WIDTH = 36

HEADLINE = """
    ____  _      _ __ 
   / __ \(_)  __(_) /_
  / / / / / |/_/ / __/
 / /_/ / />  </ / /_  
/_____/_/_/|_/_/\__/  
"""

HEADLINE = str(HEADLINE)

BANNER_MESSAGE_TOKENS = [
    ('class:title', HEADLINE),
    ('', '\n'),
]

WELCOME_MESSAGE_TOKENS = [
    ('', 'Still experimental\n\n'),
    ('', 'version '), ('class:version', dixit_version),
    ('', ', prompt_toolkit '), ('class:version', prompt_toolkit.__version__),
    ('', '\n'),
    ('', 'by Danny Waser\n\n'),
    ('class:key', '[Q]'),
    ('', '            to Quit\n'),
    ('class:key', '[Ctrl] + [O]'),
    ('', '      to Open a file\n\n'),
    ('', 'All feedback is appreciated.\n\n'),
    ('class:pythonversion', ' %s %i.%i.%i ' % (
        platform.python_implementation(),
        version[0], version[1], version[2])),
]

WELCOME_MESSAGE_HEIGHT = ''.join(t[1] for t in WELCOME_MESSAGE_TOKENS).count('\n') + 1