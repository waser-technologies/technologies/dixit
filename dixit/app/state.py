class ApplicationState:
	"""
	Application state.
	For the simplicity, we store this as a global, but better would be to
	instantiate this as an object and pass at around.
	"""

	current_path_text = None
	current_path_dxt = None

	file_transcribed = False
	file_sentences_transcribed = 0
	file_total_sentences = 0

	is_stt_auth = False
	
	dxt = {
		'index': 0,
		'is_fully_transcribed': False,
		'sentences': [
			{
				'sentence':"One sentence",
				'timestamps':[
						{'word':"One", 'starts':0, 'duration':1}
					]
				}
			]
		}

	last_sentence = None
	current_spoken_sentence = None
	current_not_spoken_sentence = None
	next_sentence = None
