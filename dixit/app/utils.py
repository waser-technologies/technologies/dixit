import os

from dixit.app.state import ApplicationState
from dixit.cli.interactive import show_message, show_dialog_as_float

try:
    wxh = os.get_terminal_size()
    is_terminal_simated = False
    WIDTH = int(wxh.columns)
    HEIGTH = int(wxh.lines)
except OSError as ose:
    wxh = None
    is_terminal_simated = True
    WIDTH = 0
    HEIGTH = 0

def path_is_valid(p):
	return os.path.exists(p)

def file_is_empty(f):
	if os.stat(f).st_size >= 0:
		return False
	return True

def file_is_not_empty(f):
	return not file_is_empty(f)

def get_dxt_path():
	if ApplicationState.current_path_text:
		return str(ApplicationState.current_path_text + ".dxt")
	else:
		return ApplicationState.current_path_dxt

def load_dxt():
	if path_is_valid(ApplicationState.current_path_dxt):
		ApplicationState.dxt = get_dxt_from_file()
		return
	else:
		return

def open_file(file_to_read):
	if file_to_read is not None:
		try:
			with open(file_to_read, "rb") as f:
				return f.read().decode("utf-8", errors="ignore")
		except OSError as e:
			show_message("Error", f"{e}")
	return None

def centered(string_lines):
	total_width = WIDTH  # get_app().output.get_size().columns
	centered_string_lines = ""

	for l in string_lines.split("\n"):
		centered_string_lines += l.center(total_width) + "\n"

	return centered_string_lines
