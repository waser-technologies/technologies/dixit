from __future__ import unicode_literals

from prompt_toolkit.application import get_app
from prompt_toolkit.filters import Condition, has_focus, vi_insert_mode, vi_navigation_mode
from prompt_toolkit.key_binding import KeyBindings

import os

__all__ = (
    'create_key_bindings',
)


def _current_window_for_event(event):
    """
    Return the `Window` for the currently focussed Buffer.
    """
    return event.app.layout.current_window


def create_key_bindings(editor):
    """
    Create custom key bindings.
    This starts with the key bindings, defined by `prompt-toolkit`, but adds
    the ones which are specific for the editor.
    """
    kb = KeyBindings()

    
    @kb.add('c-t')
    def _(event):
        """
        Override default behaviour of prompt-toolkit.
        (Control-T will swap the last two characters before the cursor, because
        that's what readline does.)
        """
        pass

    @kb.add('f1')
    def show_help(event):
        editor.show_help()

    @kb.add('c-o')
    def open_path(event):
        """
        Open file/directory in file explorer mode.
        """
        name_under_cursor = event.current_buffer.document.current_line
        new_path = os.path.normpath(os.path.join(
            editor.current_editor_buffer.location, name_under_cursor))

        editor.window_arrangement.open_buffer(
            new_path, show_in_current_window=True)
        editor.sync_with_prompt_toolkit()
    
    @kb.add("q")
    @kb.add("c-c")
    @kb.add("c-q")
    def _(event):
        "Quit when [Q] or [Ctrl] + [C|Q] is pressed."
        #PROC.kill()
        event.app.exit()

    return kb
