from __future__ import unicode_literals

HELP_TEXT = """\
Dixit Help
==========
Dixit is an editor to record words .

- [Ctrl] + [O]:    to Open text file
- [Ctrl] + [S]:    to Enable Speech recognition
- [Q] or [Ctrl] + [C or Q]:    to Quit
"""