"""
The main editor class.
Usage::
    files_to_edit = ['file1.txt', 'file2.py']
    e = Editor(files_to_edit)
    e.run()  # Runs the event loop, starts interaction.
"""
from __future__ import unicode_literals

from prompt_toolkit.application import Application
from prompt_toolkit.buffer import Buffer
from prompt_toolkit.enums import EditingMode
from prompt_toolkit.filters import Condition
from prompt_toolkit.history import FileHistory
from prompt_toolkit.key_binding.vi_state import InputMode
from prompt_toolkit.styles import DynamicStyle

from .help import HELP_TEXT
from .key_bindings import create_key_bindings
from .layout import DxtLayout, get_terminal_title
from .style import generate_built_in_styles, get_editor_style_by_name
from .window_arrangement import WindowArrangement
from .io import FileIO, HttpIO, GZipFileIO, DxtIO, DirectoryIO

import pygments
import os

__all__ = (
    'Dxt',
)


class Dxt(object):
    """
    The main class. Containing the whole editor.
    :param config_directory: Place where configuration is stored.
    :param input: (Optionally) `prompt_toolkit.input.Input` object.
    :param output: (Optionally) `prompt_toolkit.output.Output` object.
    """
    def __init__(self, config_directory='~/.dxtrc', input=None, output=None):
        self.input = input
        self.output = output

        
        # Ensure config directory exists.
        self.config_directory = os.path.abspath(os.path.expanduser(config_directory))
        if not os.path.exists(self.config_directory):
            os.mkdir(self.config_directory)

        self.window_arrangement = WindowArrangement(self)
        self.message = None

        # Load styles. (Mapping from name to Style class.)
        self.styles = generate_built_in_styles()
        self.current_style = get_editor_style_by_name('dixit')

        # I/O backends.
        self.io_backends = [
            DirectoryIO(),
            HttpIO(),
            GZipFileIO(),  # Should come before FileIO.
            FileIO(),
            DxtIO(),
        ]

        # Create key bindings registry.
        self.key_bindings = create_key_bindings(self)

        # Create layout and CommandLineInterface instance.
        self.editor_layout = DxtLayout(self, self.window_arrangement)
        self.application = self._create_application()


    def load_initial_files(self, locations):
        """
        Load a list of files.
        """

        # When no files were given, open at least one empty buffer.
        locations2 = locations or [None]

        for f in locations2:
            self.window_arrangement.open_buffer(f)

        if locations and len(locations) > 1:
            self.show_message('%i files loaded.' % len(locations))

    def _create_application(self):
        """
        Create CommandLineInterface instance.
        """
        # Create Application.
        application = Application(
            input=self.input,
            output=self.output,
            layout=self.editor_layout.layout,
            key_bindings=self.key_bindings,
            #get_title=lambda: get_terminal_title(self),
            style=DynamicStyle(lambda: self.current_style),
#            ignore_case=Condition(lambda: self.ignore_case),  # TODO
            include_default_pygments_style=False,
            mouse_support=True,
            full_screen=True,
            enable_page_navigation_bindings=True)

        return application

    @property
    def current_editor_buffer(self):
        """
        Return the `DxtEditorBuffer` that is currently active.
        """
        current_buffer = self.application.current_buffer

        # Find/return the EditorBuffer with this name.
        for b in self.window_arrangement.editor_buffers:
            if b.buffer == current_buffer:
                return b

    @property
    def add_key_binding(self):
        """
        Shortcut for adding new key bindings.
        (Mostly useful for a dxtrc file, that receives this Editor instance
        as input.)
        """
        return self.key_bindings.add

    def show_message(self, message):
        """
        Set a warning message. The layout will render it as a "pop-up" at the
        bottom.
        """
        self.message = message

    def sync_with_prompt_toolkit(self):
        """
        Update the prompt-toolkit Layout and FocusStack.
        """
        # After executing a command, make sure that the layout of
        # prompt-toolkit matches our WindowArrangement.
        self.editor_layout.update()

        # Make sure that the focus stack of prompt-toolkit has the current
        # page.
        window = self.window_arrangement.active_pt_window
        if window:
            self.application.layout.focus(window)

    def show_help(self):
        """
        Show help in new window.
        """
        self.window_arrangement.hsplit(text=HELP_TEXT)
        self.sync_with_prompt_toolkit()  # Show new window.

    def run(self):
        """
        Run the event loop for the interface.
        This starts the interaction.
        """
        # Make sure everything is in sync, before starting.
        self.sync_with_prompt_toolkit()

        # Run eventloop of prompt_toolkit.
        self.application.run()