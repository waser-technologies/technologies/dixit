#!/usr/bin/env python
"""
Dixit: STT timestamps recorder.
Usage:
    dixit [-u <dxtrc>] [<location>...]

Options:
    -u <dxtrc> : Use this .dxtrc file instead.
"""
from __future__ import unicode_literals
import docopt
import os

from dixit.editor import Dxt
from dixit.rc_file import run_rc_file

__all__ = (
    'run',
)


def run():
    a = docopt.docopt(__doc__)
    locations = a['<location>']
    dxtrc = a['-u']

    # Create new dxt instance.
    editor = Dxt()

    # Apply rc file.
    if dxtrc:
        run_rc_file(editor, dxtrc)
    else:
        default_dxtrc = os.path.expanduser('~/.dxtrc')

        if os.path.exists(default_dxtrc):
            run_rc_file(editor, default_dxtrc)

    # Load files and run.
    editor.load_initial_files(locations)
    editor.run()


if __name__ == '__main__':
    run()