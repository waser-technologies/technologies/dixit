from prompt_toolkit.key_binding import KeyBindings

#  Key bindings.
bindings = KeyBindings()

@bindings.add("c-a")
def _(event):
	"Focus menu."
	event.app.layout.focus(current_sentence_not_spoken)

@bindings.add("q")
@bindings.add("c-c")
@bindings.add("c-q")
def _(event):
	"Quit when [Q] or [Ctrl] + [C|Q] is pressed."
	#PROC.kill()
	event.app.exit()

@bindings.add("c-o")
def _(event):
	"Open file"
	do_open_file()
