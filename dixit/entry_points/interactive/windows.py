from prompt_toolkit.layout.containers import Window, WindowAlign

from dixit.cli.interactive.texts import (
    banner_text,
    tips_text,
    past_sentence,
    current_sentence_spoken,
    current_sentence_not_spoken,
    future_sentence,
    toolbar
)

banner = Window(banner_text, style="class:last_sentence")
tips=Window(tips_text, height=2)
past=Window(past_sentence, height=2, style="class:last_sentence")
current_spoken=Window(current_sentence_spoken, height=2, style="class:spoken", align=WindowAlign.RIGHT)
current_not_spoken=Window(current_sentence_not_spoken, height=2, style="class:not_spoken", align=WindowAlign.LEFT)
future=Window(future_sentence, height=2, style="class:next_sentence")
toolbar=Window(toolbar, height=1, style="bg:#FF7600 #000000")