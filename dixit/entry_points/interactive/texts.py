from prompt_toolkit.layout.controls import FormattedTextControl
from prompt_toolkit import print_formatted_text, HTML

from dixit.app.state import ApplicationState
from dixit.app.utils import centered


HEADLINE = """
    ____  _      _ __ 
   / __ \(_)  __(_) /_
  / / / / / |/_/ / __/
 / /_/ / />  </ / /_  
/_____/_/_/|_/_/\__/  
"""

def get_tip():
	if not ApplicationState.current_path_text:
		return centered("Open a text file to pronounce it ([Ctrl] + [O])")
	elif not ApplicationState.is_stt_auth:
		return centered("To pronounce something you need to enable STT ([Ctrl] + [S])")
	else:
		return centered("Start reading out loud")

def get_past_sentence():
	if not ApplicationState.current_path_text:
		return ""
	else:
		if ApplicationState.last_sentence:
			return centered(ApplicationState.last_sentence)

def get_current_sentence_spoken():
	return "Just like that,"

def get_current_sentence_not_spoken():
	return " " + "you can transcribe what I say."

def get_future_sentence():
	return centered("All night long!")

def get_toolbar():
	return HTML(
		"<bottom_toolbar>" + centered("([Ctrl]) + [Q]uit | [Ctrl] + [S]TT | [Ctrl] + [O]pen text file").replace("\n", "") + "</bottom_toolbar>"
	)

banner_text = FormattedTextControl(
		text=centered(HEADLINE),
		focusable=False
	)

tips_text = FormattedTextControl(
		text=get_tip,
		focusable=False
	)

past_sentence = FormattedTextControl(
		text=get_past_sentence,
		focusable=False
	)

current_sentence_spoken = FormattedTextControl(
		text=get_current_sentence_spoken,
		focusable=False,
	)

current_sentence_not_spoken = FormattedTextControl(
		text=get_current_sentence_not_spoken,
		focusable=True
	)

future_sentence = FormattedTextControl(
		text=get_future_sentence,
		focusable=False
	)

toolbar = FormattedTextControl(
		text=get_toolbar,
		focusable=False
	)