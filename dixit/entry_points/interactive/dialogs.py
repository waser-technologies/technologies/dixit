from asyncio import Future, ensure_future
from prompt_toolkit.widgets import Button, Dialog
from prompt_toolkit.layout import HSplit
from prompt_toolkit.widgets import Label
from prompt_toolkit.layout.dimension import LayoutDimension as D
from prompt_toolkit.layout.containers import Float

class TextInputDialog:
	def __init__(self, title="", label_text="", completer=None):
		self.future = Future()

		def accept_text(buf):
			get_app().layout.focus(ok_button)
			buf.complete_state = None
			return True

		def accept():
			self.future.set_result(self.text_area.text)

		def cancel():
			self.future.set_result(None)

		self.text_area = TextArea(
			completer=completer,
			multiline=False,
			width=D(preferred=40),
			accept_handler=accept_text,
		)

		ok_button = Button(text="OK", handler=accept)
		cancel_button = Button(text="Cancel", handler=cancel)

		self.dialog = Dialog(
			title=title,
			body=HSplit([Label(text=label_text), self.text_area]),
			buttons=[ok_button, cancel_button],
			width=D(preferred=80),
			modal=True,
		)

	def __pt_container__(self):
		return self.dialog


class MessageDialog:
	def __init__(self, title, text):
		self.future = Future()

		def set_done():
			self.future.set_result(None)

		ok_button = Button(text="OK", handler=(lambda: set_done()))

		self.dialog = Dialog(
			title=title,
			body=HSplit([Label(text=text)]),
			buttons=[ok_button],
			width=D(preferred=80),
			modal=True,
		)

	def __pt_container__(self):
		return self.dialog

class YesNoDialog:
	def __init__(self, title, text, yes_text, no_text):
		self.future = Future()

		def set_true():
			self.future.set_result(True)
		
		def set_false():
			self.future.set_result(False)


		yes_text = yes_text or "Yes"
		no_text = no_text or "No"

		yes_button = Button(text=yes_text, handler=set_true)
		no_button = Button(text=no_text, handler=set_false)

		self.dialog = Dialog(
			title=title,
			body=HSplit([Label(text=text)]),
			buttons=[yes_button, no_button],
			width=D(preferred=80),
			modal=True,
		)

	def __pt_container__(self):
		return self.dialog