from asyncio import ensure_future
from prompt_toolkit.application import Application
from prompt_toolkit.layout import Layout

from .styles import style
from .bindings import bindings

# create application.

from prompt_toolkit.layout import Dimension, HSplit, VSplit, HorizontalAlign, VerticalAlign
from prompt_toolkit.widgets import MenuContainer, MenuItem
from prompt_toolkit.layout.menus import CompletionsMenu
from prompt_toolkit.completion import PathCompleter
from prompt_toolkit.layout.containers import Float

from dixit.cli.interactive.dialogs import MessageDialog
from dixit.cli.interactive.texts import *
from dixit.cli.interactive.windows import *

from dixit.cli.interactive.bindings import bindings

from dixit.cli.interactive.handlers import *
from dixit.cli.interactive.icons import *


body = HSplit(
	[
		VSplit(
			[
				HSplit(
					[	
						# Banner
						windows.banner,
						# Tips
						windows.tips,

						# Past sentences
						windows.past,
						
						VSplit(
							[
								# Current sentences
								windows.current_spoken,
								windows.current_not_spoken
							],
						),
						# Future sentences
						windows.future

					],
					align=VerticalAlign.TOP,
				),
			],
			align=HorizontalAlign.CENTER,
		),
		windows.toolbar
	],
	align=VerticalAlign.BOTTOM
)

root_container = MenuContainer(
	body=body,
	menu_items=[
		MenuItem(
			"[Dixit]",
			children=[
				MenuItem(INFO_ICON + " About", handler=do_about),
				MenuItem("-", disabled=True),
				MenuItem(INFO_ICON + " Print environment", handler=do_print_env),
				MenuItem(EXIT_ICON + " Quit", handler=do_exit),
			],
		),
		MenuItem(
			"[File]",
			children=[
				MenuItem(FILE_ICON + " Open file ([Ctrl] + [O])", handler=do_open_file),
			],
		),
		MenuItem(
			"[STT]",
			children=[
				MenuItem(MIC_ICON + " Enable STT ([Ctrl] + [S])", handler=do_about),
			],
		),
		MenuItem(
			"[Help]",
			children=[
				MenuItem(MANUAL_ICON + " Manual", handler=do_manual),
				MenuItem(HELP_ICON + " How can Dixit help me?",
						handler=do_how_help),
				MenuItem("Keyboard Shortcuts", do_shortcuts)
			]
		)
	],
	floats=[
		Float(
			xcursor=True,
			ycursor=True,
			content=CompletionsMenu(max_height=3, scroll_offset=1),
		),
	],
	key_bindings=bindings
)

layout = Layout(root_container, focused_element=current_sentence_not_spoken)

application = Application(
	layout=layout,
	key_bindings=bindings,
	enable_page_navigation_bindings=True,
	mouse_support=True,
	style=style,
	full_screen=True,
	refresh_interval=1
)

async def show_dialog_as_float(dialog):
	"Coroutine."
	float_ = Float(content=dialog)
	root_container.floats.insert(0, float_)

	app = get_app()

	focused_before = app.layout.current_window
	app.layout.focus(dialog)
	result = await dialog.future
	app.layout.focus(focused_before)

	if float_ in root_container.floats:
		root_container.floats.remove(float_)
	
	return result

def show_message(title, text):
	async def coroutine():
		dialog = MessageDialog(title, text)
		await show_dialog_as_float(dialog)

	ensure_future(coroutine())

application.run()
