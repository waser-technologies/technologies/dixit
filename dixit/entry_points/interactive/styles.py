from prompt_toolkit.styles import Style

style = Style.from_dict(
	{
		"spoken": "#FFA900",
		"last_sentence": "#FF7600",
		"not_spoken": "lightgray",
		"next_sentence": "gray",
		"window.border": "#FF7600",
		"menu-bar": "bg:#FF7600 #000000",
		"menu-bar.selected-item": "bg:#FFA900 #000000",
		"menu": "bg:#FF7600 #000000",
		"menu.border": "#aaaaaa",
		"window.border shadow": "#52006A",
		"focused  button": "bg:#52006A #FFA900 noinherit",
		# Styling for Dialog widgets.
		"button-bar": "bg:#52006A",
		"status": "bg:#FF7600",
		"shadow": "bg:#52006A",
		'dialog': 'bg:#52006A',
		'dialog frame.label': 'bg:#FF7600 #000000',
		'dialog.body':  'bg:#FF7600 #000000',
		'dialog shadow': 'bg:#52006A',
	}
)