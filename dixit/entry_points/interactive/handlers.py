from asyncio import ensure_future
from prompt_toolkit.layout.containers import Float

from dixit.app.state import ApplicationState
from dixit.cli.interactive.dialogs import MessageDialog
from dixit.cli.interactive.containers import show_message, show_dialog_as_float

def do_exit():
	get_app().exit()

def do_about():
	show_message("About Dixit",
				"Dixit version %s.\nCreated with love by Danny Waser." % "a0.1.0")

def do_print_env():
	env = "\n".join([
		"Text Path: " + str(ApplicationState.current_path_text),
		"Dxt Path: " + str(ApplicationState.current_path_dxt),
		"Text transcribed: " + str(ApplicationState.file_transcribed) + " (" + str(int(ApplicationState.file_sentences_transcribed / (ApplicationState.file_total_sentences or 1))) + " %)",
		"STT Auth: " + str(ApplicationState.is_stt_auth),
		#"Dxt: " + str(ApplicationState.dxt)
	])

	show_message("Environ", str(env))

def do_manual():
	show_message("Manual", "This is a boring manual.")

def do_how_help():
	show_message("How can Dixit help?", "Give dixit a file and it will record, timestamp and save your speech.\nThis will allow you to get examples of your voice to train a custom model.")

def do_shortcuts():
	kbshortcuts_txt = HTML("[Tab] -> Focus next element\n[Ctrl] + [A] -> Open menu\n[Ctrl] + ([Q] or [C]) -> Quit\n[S] + [Enter] -> Open menu")
	show_message("Keyboard Shortcuts", kbshortcuts_txt)

def do_open_file():
	async def coroutine():
		open_dialog = TextInputDialog(
			title="Open text file",
			label_text="Enter the path to a text file:",
			completer=PathCompleter(),
		)

		path = await show_dialog_as_float(open_dialog)
		
		if (not path) or (not path_is_valid(path)) or (file_is_empty(path)):
			show_message("{i}: Not a valid text file", "The path you entered either does not exist or points to an empty file.")
		else:
			ApplicationState.current_path_text = path
			ApplicationState.current_path_dxt = get_dxt_path()
	
	ensure_future(coroutine())
