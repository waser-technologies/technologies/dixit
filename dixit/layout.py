"""
The actual layout for the renderer.
"""
from __future__ import unicode_literals
from prompt_toolkit.application.current import get_app
from prompt_toolkit.filters import has_focus, is_searching, Condition, has_arg
from prompt_toolkit.key_binding.vi_state import InputMode
from prompt_toolkit.layout import HSplit, VSplit, FloatContainer, Float, Layout, VerticalAlign, HorizontalAlign
from prompt_toolkit.layout.containers import Window, ConditionalContainer, ColorColumn, WindowAlign, ScrollOffsets
from prompt_toolkit.layout.controls import BufferControl
from prompt_toolkit.layout.controls import FormattedTextControl
from prompt_toolkit.layout.dimension import Dimension
from prompt_toolkit.layout.margins import ConditionalMargin, NumberedMargin
from prompt_toolkit.layout.menus import CompletionsMenu
from prompt_toolkit.layout.processors import Processor, ConditionalProcessor, BeforeInput, ShowTrailingWhiteSpaceProcessor, Transformation, HighlightSelectionProcessor, HighlightSearchProcessor, HighlightIncrementalSearchProcessor, HighlightMatchingBracketProcessor, TabsProcessor, DisplayMultipleCursors
from prompt_toolkit.layout.utils import explode_text_fragments
from prompt_toolkit.mouse_events import MouseEventType
from prompt_toolkit.selection import SelectionType
from prompt_toolkit.widgets.toolbars import FormattedTextToolbar, SystemToolbar, SearchToolbar, ValidationToolbar, CompletionsToolbar
from prompt_toolkit.widgets import MenuContainer, MenuItem

from .welcome_message import WELCOME_MESSAGE_TOKENS, WELCOME_MESSAGE_HEIGHT, WELCOME_MESSAGE_WIDTH, BANNER_MESSAGE_TOKENS

import dixit.window_arrangement as window_arrangement
from functools import partial

import re
import sys

__all__ = (
    'DxtLayout',
    'get_terminal_title',
)

def _try_char(character, backup, encoding=sys.stdout.encoding):
    """
    Return `character` if it can be encoded using sys.stdout, else return the
    backup character.
    """
    if character.encode(encoding, 'replace') == b'?':
        return backup
    else:
        return character


class BannerMessageWindow(Window):
    """
    Banner
    """
    def __init__(self, editor):

        super(BannerMessageWindow, self).__init__(
            Window(
                FormattedTextControl(lambda: BANNER_MESSAGE_TOKENS),
                align=WindowAlign.CENTER,
                style="class:title"),
                )

class WelcomeMessageWindow(ConditionalContainer):
    """
    Welcome message pop-up, which is shown during start-up when no other files
    were opened.
    """
    def __init__(self, editor):
        once_hidden = [False]  # Nonlocal

        def condition():
            # Get editor buffers
            buffers = editor.window_arrangement.editor_buffers

            # Only show when there is only one empty buffer, but once the
            # welcome message has been hidden, don't show it again.
            result = (len(buffers) == 1 and buffers[0].buffer.text == '' and
                      buffers[0].location is None and not once_hidden[0])
            if not result:
                once_hidden[0] = True
            return result

        super(WelcomeMessageWindow, self).__init__(
            Window(
                FormattedTextControl(lambda: WELCOME_MESSAGE_TOKENS),
                align=WindowAlign.CENTER,
                style="class:welcome"),
            filter=Condition(condition))


class MessageToolbarBar(ConditionalContainer):
    """
    Pop-up (at the bottom) for showing error/status messages.
    """
    def __init__(self, editor):
        def get_tokens():
            if editor.message:
                return [('class:message', editor.message)]
            else:
                return []

        super(MessageToolbarBar, self).__init__(
            FormattedTextToolbar(get_tokens),
            filter=Condition(lambda: editor.message is not None))

class DxtScrollOffsets(ScrollOffsets):
    def __init__(self, editor):
        self.editor = editor
        self.left = 0
        self.right = 0

    @property
    def top(self):
        return self.editor.scroll_offset

    @property
    def bottom(self):
        return self.editor.scroll_offset


class DxtLayout(object):
    """
    The main layout class.
    """
    def __init__(self, editor, window_arrangement):
        self.editor = editor  # Back reference to editor.
        self.window_arrangement = window_arrangement

        self._frames = {}

        self._fc = FloatContainer(
            content=VSplit([
                Window(BufferControl()),  # Dummy window
            ]),
            floats=[
                Float(bottom=1, left=0, right=0, height=1,
                      content=MessageToolbarBar(editor)),
                Float(content=WelcomeMessageWindow(editor),
                      height=WELCOME_MESSAGE_HEIGHT,
                      width=WELCOME_MESSAGE_WIDTH),
            ]
        )

        self.layout = Layout(MenuContainer(
            body=HSplit([
                VSplit(
                    [
                        HSplit([
                            self._fc
                        ], align=VerticalAlign.TOP )
                    ],
                    align=HorizontalAlign.CENTER
                ),
                SystemToolbar(),
            ], align=VerticalAlign.BOTTOM),
            menu_items=[
                MenuItem(
                    "[Dixit]",
                    children=[
                        MenuItem("About", handler=self.editor.show_help),
                        MenuItem("-", disabled=True),
                        MenuItem("Print environment", handler=self.editor.show_help),
                        MenuItem("Quit", handler=self.exit),
                    ],
                ),
                MenuItem(
                    "[File]",
                    children=[
                        MenuItem("Open file ([Ctrl] + [O])", handler=self.editor.show_help),
                    ],
                ),
                MenuItem(
                    "[STT]",
                    children=[
                        MenuItem("Enable STT ([Ctrl] + [S])", handler=self.editor.show_help),
                    ],
                ),
                MenuItem(
                    "[Help]",
                    children=[
                        MenuItem("Manual", handler=self.editor.show_help),
                        MenuItem("How can Dixit help me?",
                                handler=self.editor.show_help),
                        MenuItem("Keyboard Shortcuts", self.editor.show_help)
                    ]
                )
            ],
        ))

    def get_vertical_border_char(self):
        " Return the character to be used for the vertical border. "
        return _try_char('\u2502', '|', get_app().output.encoding())

    def update(self):
        """
        Update layout to match the layout as described in the
        WindowArrangement.
        """
        # Start with an empty frames list everytime, to avoid memory leaks.
        existing_frames = self._frames
        self._frames = {}

        def create_layout_from_node(node):
            if isinstance(node, window_arrangement.Window):
                # Create frame for Window, or reuse it, if we had one already.
                key = (node, node.editor_buffer)
                frame = existing_frames.get(key)
                if frame is None:
                    frame, pt_window = self._create_window_frame(node.editor_buffer)

                    # Link layout Window to arrangement.
                    node.pt_window = pt_window

                self._frames[key] = frame
                return frame

            elif isinstance(node, window_arrangement.VSplit):
                return VSplit(
                    [create_layout_from_node(n) for n in node],
                    padding=1,
                    padding_char=self.get_vertical_border_char(),
                    padding_style='class:frameborder')

            if isinstance(node, window_arrangement.HSplit):
                return HSplit([create_layout_from_node(n) for n in node])

        layout = create_layout_from_node(self.window_arrangement.active_tab.root)
        self._fc.content = layout

        def _create_window_frame(self, editor_buffer):
            """
            Create a Window for the buffer, with underneat a status bar.
            """
            @Condition
            def wrap_lines():
                return self.editor.wrap_lines

            window = Window(
                self._create_buffer_control(editor_buffer),
                allow_scroll_beyond_bottom=True,
                scroll_offsets=ScrollOffsets(
                    left=0, right=0,
                    top=(lambda: self.editor.scroll_offset),
                    bottom=(lambda: self.editor.scroll_offset)),
                wrap_lines=wrap_lines,
                left_margins=[ConditionalMargin(
                        margin=NumberedMargin(
                            display_tildes=True,
                            relative=Condition(lambda: self.editor.relative_number)),
                        filter=Condition(lambda: self.editor.show_line_numbers))],
                cursorline=Condition(lambda: self.editor.cursorline),
                cursorcolumn=Condition(lambda: self.editor.cursorcolumn),
                colorcolumns=(
                    lambda: [ColorColumn(pos) for pos in self.editor.colorcolumn]),
                ignore_content_width=True,
                ignore_content_height=True,
                get_line_prefix=partial(self._get_line_prefix, editor_buffer.buffer))

            return HSplit([
                window,
                VSplit([
                    WindowStatusBar(self.editor, editor_buffer),
                    WindowStatusBarRuler(self.editor, window, editor_buffer.buffer),
                ], width=Dimension()),  # Ignore actual status bar width.
            ]), window

        def _create_buffer_control(self, editor_buffer):
            """
            Create a new BufferControl for a given location.
            """
            @Condition
            def preview_search():
                return self.editor.incsearch

            input_processors = [
                # Processor for visualising spaces. (should come before the
                # selection processor, otherwise, we won't see these spaces
                # selected.)
                ConditionalProcessor(
                    ShowTrailingWhiteSpaceProcessor(),
                    Condition(lambda: self.editor.display_unprintable_characters)),

                # Replace tabs by spaces.
                TabsProcessor(
                    tabstop=(lambda: self.editor.tabstop),
                    char1=(lambda: '|' if self.editor.display_unprintable_characters else ' '),
                    char2=(lambda: _try_char('\u2508', '.', get_app().output.encoding())
                                        if self.editor.display_unprintable_characters else ' '),
                ),

                # Reporting of errors, for Pyflakes.
                ReportingProcessor(editor_buffer),
                HighlightSelectionProcessor(),
                ConditionalProcessor(
                    HighlightSearchProcessor(),
                    Condition(lambda: self.editor.highlight_search)),
                ConditionalProcessor(
                    HighlightIncrementalSearchProcessor(),
                    Condition(lambda: self.editor.highlight_search) & preview_search),
                HighlightMatchingBracketProcessor(),
                DisplayMultipleCursors(),
            ]

            return BufferControl(
                lexer=DocumentLexer(editor_buffer),
                include_default_input_processors=False,
                input_processors=input_processors,
                buffer=editor_buffer.buffer,
                preview_search=preview_search,
                search_buffer_control=self.search_control,
                focus_on_click=True)

        def _get_line_prefix(self, buffer, line_number, wrap_count):
            if wrap_count > 0:
                result = []

                # Add 'breakindent' prefix.
                if self.editor.break_indent:
                    line = buffer.document.lines[line_number]
                    prefix = line[:len(line) - len(line.lstrip())]
                    result.append(('', prefix))

                # Add softwrap mark.
                result.append(('class:soft-wrap', '...'))
                return result
            return ''

    def exit(self):
        try:
            get_app().exit()
        except:
            exit()


def get_terminal_title(editor):
    """
    Return the terminal title,
    e.g.: "filename.py (/directory) - Dixit"
    """
    eb = editor.current_editor_buffer
    if eb is not None:
        return '%s - Dixit' % (eb.location or '[New file]', )
    else:
        return 'Dixit'