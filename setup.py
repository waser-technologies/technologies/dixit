#!/usr/bin/env python
import os
from setuptools import setup, find_packages
import dixit

with open(os.path.join(os.path.dirname(__file__), 'README.md')) as f:
    long_description = f.read()


setup(
    name='dixit',
    author='Danny Waser',
    version=dixit.__version__,
    license='LICENSE',
    url='https://gitlab.com/wasertech/technologies/dixit',
    description='Record yourself while you read out loud',
    long_description=long_description,
    packages=find_packages('.'),
    install_requires = [
        'prompt_toolkit>=2.0.0,<3.1.0',
        'six',
        'pyflakes',        # For Python error reporting.
        'pygments',        # For the syntax highlighting.
        'docopt',          # For command line arguments.
    ],
    entry_points={
        'console_scripts': [
            'dixit = dixit.entry_points.run_dixit:run',
        ]
    },
)