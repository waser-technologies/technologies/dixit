# Dixit

> _DIDACTIC_ or _IRONIC_ preposition

Used before or after the name of someone whose words are reported, to emphasize that these are his own words.

`dixit` allows you to quickly and easely record yourself while reading a piece of text out loud.

Once you have said at least one sentence (one line in the text file), `dixit` creates a new file `.dxt` file next to the `.txt`.


## Getting started

```zsh


                                _____        __  
                               /__  /  _____/ /_ 
                                 / /  / ___/ __ \
                                / /__(__  ) / / /
                               /____/____/_/ /_/ 


                                 Qui y a-t-il ?
❯ cat const-ch-fr.txt | head
Constitution fédérale de la Confédération suisse
Préambule
Au nom de Dieu Tout-Puissant!
Le peuple et les cantons suisses
de leur responsabilité envers la Création
résolus à renouveler leur alliance pour renforcer la liberté
la démocratie
l’indépendance et la paix dans un esprit de solidarité et d’ouverture au monde
déterminés à vivre ensemble leurs diversités
le respect de l’autre et l’équité

dixit --help
usage: dixit [text_file|dxt_file]

Easely record yourself reading a text file and play it back with timestamps.

# record and transcribe with timestamps while you read out lout
dixit const_ch_fr.txt 
# playback the results
dixit const_ch_fr.dxt
```

